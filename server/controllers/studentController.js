  const Student = require("../models/studentModel");
const e = require("express");

/**
 * Creates a student
 *
 * @param {*} req
 * @param {*} res
 */
const studentPost = (req, res) => {
  var student = new Student();

  student.firstname = req.body.firstname;
  student.lastname = req.body.lastname;
  student.email = req.body.email;
  student.address = req.body.address;


  if (student.firstname && student.lastname && student.email && student.address) {
    student.save(function (err) {
      if (err) {
        res.status(422);
        console.log('Error while saving the student', err)
        res.json({
          error: 'There was an error saving the student'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/students/?id=${student.id}`
      });
      res.json(student);
    });
  } else {
    res.status(422);
    console.log('Error while saving the student')
    res.json({
      error: 'No valid data provided for student'
    });
  }
};

/**
 * Get all students
 *
 * @param {*} req
 * @param {*} res
 */
const studentGet = (req, res) => {
  // if an specific student is required
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the student', err)
        res.json({ error: "Student doesnt exist" })
      }
      res.json(student);
    });
  } else {
    // get all students
    Student.find(function (err, students) {
      if (err) {
        res.status(422);
        res.json({ "Error": err });
      }
      res.json(students);
    });

  }
};

/**
 * Updates a student
 *
 * @param {*} req
 * @param {*} res
 */
const studentPatch = (req, res) => {
  // get student by id
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the student', err)
        res.json({ error: "Student doesnt exist" })
      }

      student.firstname = req.body.firstname ? req.body.firstname : student.firstname;
      student.lastname = req.body.lastname ? req.body.lastname : student.lastname;
      student.email = req.body.email ? req.body.email : student.email;
      student.address = req.body.address ? req.body.address : student.address;

      student.save(function (err) {
        if (err) {
          res.status(422);
          console.log('Error while saving the student', err)
          res.json({
            error: 'There was an error saving the student'
          });
        }
        res.status(200); // OK
        res.json(student);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "Student doesnt exist" })
  }
};

/**
 * Delete a student
 *
 * @param {*} req
 * @param {*} res
 */

const studentDelete = (req, res) => {
  if (req.query && req.query.id) {
    Student.findById(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the student', err)
        res.json({ error: "Student doesnt exist" })
      }
      
      
        
      student.remove(function (err){
          if(err){
            res.status(500).send({message: 'Error al borrar el student: ${err}'})
          } 
          res.status(200).send({message: 'El student ha sido eliminado'})

        })
      
     
    });
  } else {
    res.status(404);
    res.json({ error: "Student doesnt exist" })
  }
};

module.exports = {
  studentGet,
  studentPost,
  studentPatch,
  studentDelete
}