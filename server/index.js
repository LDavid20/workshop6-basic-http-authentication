const express = require('express');
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/todo-api");
const {
  base64decode
} = require('nodejs-base64');
const cors = require("cors");
const bodyParser = require("body-parser");


const {
  taskPatch,
  taskPost,
  taskGet,
  taskDelete,
} = require("./controllers/taskController.js");
const {
  studentPatch,
  studentPost,
  studentGet,
  studentDelete,
} = require("./controllers/studentController.js");

const {
  saveSession
} = require("./controllers/sessionController.js");



// parser for the request body (required for the POST and PUT methods)
// check for cors
app.use(cors());

// parses the body
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());


//basic authentication
app.use(function (req, res, next) {
  if (req.headers["authorization"]) {
    // Basic VVROOlBhc3N3b3JkMQ==

    const authBase64 = req.headers['authorization'].split(' ');
    console.log('authBase64:', authBase64);
    const userPass = base64decode(authBase64[1]);
    console.log('userPass:', userPass);
    const user = userPass.split(':')[0];
    const password = userPass.split(':')[1];

    if (user === 'admin' && password == '1234') {
      // saveSession('admin');
      next();
      return;
    }
  }
  res.status(401);
  res.send({
    error: "Unauthorized"
  });
});


// listen to the task request
app.get("/api/tasks", taskGet);
app.post("/api/tasks", taskPost);
app.patch("/api/tasks", taskPatch);
app.put("/api/tasks", taskPatch);
app.delete("/api/tasks", taskDelete);

app.get("/api/students", studentGet);
app.post("/api/students", studentPost);
app.patch("/api/students", studentPatch);
app.put("/api/students", studentPatch);
app.delete("/api/students", studentDelete);

// handle 404
app.use(function (req, res, next) {
  res.status(404);
  res.send({
    error: "Not found"
  });
  return;
});


app.listen(3000, () => console.log(`Example app listening on port 3000!`))
